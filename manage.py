#from flask import Flask
#from flask_sqlalchemy import SQLAlchemy
from flask_migrate import Migrate, MigrateCommand
from flask_script import Manager
from app.db_models import Users, Tasks
#import os
from app import app, db
print(type(app))
print(db)
#user = os.getenv('USERNAME')
#password = os.getenv('PASSWORD')

#app = Flask(__name__)
#app.config['SQLALCHEMY_DATABASE_URI'] = "postgresql://fregular:nueva123@localhost:5432/tareas"
#app.config['SQLALCHEMY_DATABASE_URI'] = f'postgresql://{user}:{password}@localhost:5432/tareas'
migrate = Migrate(app, db)
#db = SQLAlchemy(app)
manager = Manager(app)
manager.add_command('db', MigrateCommand)

class Users(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(40))
    password = db.Column(db.String(200))
    tasks = db.relationship('Tasks', backref='todo')

class Tasks(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    description = db.Column(db.String(300))
    done = db.Column(db.Boolean)
    user_id = db.Column(db.Integer, db.ForeignKey('users.id'))


if __name__ == "__main__":
    manager.run()

