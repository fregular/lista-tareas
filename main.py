from flask import request, make_response, session, redirect, url_for, render_template, flash,jsonify
from flask_login import login_required, current_user
from app.forms import TodoForm
from app import create_app
from app.db_models import get_todos, update_todo, put_todo, delete_todo, get_user, get_todos_full
#from app import csrf
import unittest

app = create_app()

@app.cli.command()
def test():
    tests = unittest.TestLoader().discover('tests')
    unittest.TextTestRunner().run(tests)



@app.errorhandler(404)
def not_found(error):
    return render_template('404.html', error=error)


@app.errorhandler(500)
def server_error(error):
    return render_template('500.html', error=error)

@app.route('/')
def index():
    user_ip = request.remote_addr
    response = make_response(redirect('/hello'))
    session['user_ip'] = user_ip
    return response


@app.route('/hello', methods=['GET','POST'])
@login_required
def hello():
    user_ip= session.get('user_ip')
    username = get_user(current_user.id)
    todo_form = TodoForm()

    context = {
        'user_ip': user_ip,
        'todos': get_todos(user_id=username.id),
        'username': username.name,
        'todo_form': todo_form,
        }

    if todo_form.validate_on_submit():
        put_todo(user_id=username.name, description = todo_form.description.data)
        flash('Tu tarea se creo con exito!')
        return redirect(url_for('hello'))
    return render_template('hello.html', **context)

@app.route('/todos/delete/<todo_id>', methods=['GET','POST'])
def delete(todo_id):
    user_id = current_user.id
    delete_todo(todo_id=todo_id)
    return redirect(url_for('hello'))

@app.route('/todos/update/<todo_id>/<int:done>', methods=['GET','POST'])
def update(todo_id, done):
    user_id = current_user.id
    update_todo(todo_id=todo_id, done=done)
    return redirect(url_for('hello'))

@app.route('/api/all_task', methods=['GET'])
def all_task():
    out = get_todos_full()    
    list_todo = []
    out_dict = {}
    for data in out:
        out_dict['user']= data.todo.name
        out_dict['descripción'] = data.description
        out_dict['status'] = data.done
        list_todo.append(out_dict)
        out_dict = {}
    print(list_todo)
    return jsonify(list_todo)
    
    
