import os
"""
All settings go here. The settings
are imported in app.__init__
"""
basedir = os.path.abspath(os.path.dirname(__file__))
user = os.getenv('POSTGRES_USER')
password = os.getenv('POSTGRES_PASSWORD')
server = os.getenv('POSTGRES_SERVER')
database = os.getenv('POSTGRES_DB')


class Config:
    SECRET_KEY = 'SUPER SECRET'
    PRESERVE_CONTEXT_ON_EXCEPTION = False
    SQLALCHEMY_TRACK_MODIFICATIONS = False
    SQLALCHEMY_DATABASE_URI = f'postgresql://{user}:{password}@{server}:5432/{database}'
    CSRF_ENABLED = True
    #SQLALCHEMY_DATABASE_URI = os.environ['DATABASE_URL']



class ProductionConfig(Config):
    DEBUG = False


class StagingConfig(Config):
    DEVELOPMENT = True
    DEBUG = True


class DevelopmentConfig(Config):
    DEVELOPMENT = True
    DEBUG = True


class TestingConfig(Config):
    TESTING = True
