#from app import db
from flask_sqlalchemy import SQLAlchemy

db = SQLAlchemy()

class Users(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(40))
    password = db.Column(db.String(200))
    tasks = db.relationship('Tasks', backref='todo')

class Tasks(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    description = db.Column(db.String(300))
    done = db.Column(db.Boolean)
    user_id = db.Column(db.Integer, db.ForeignKey('users.id'))

def get_users():
    return Users.query.all()

def get_user(user_id):
    return Users.query.filter_by(name=user_id).first()


def user_put(user_data):
    user = Users(name=user_data.username, password=user_data.password)
    db.session.add(user)
    db.session.commit()

def put_todo(user_id, description):
    user = Users.query.filter_by(name=user_id).first()
    task = Tasks(description=description, todo=user, done=False)
    db.session.add(task)
    db.session.commit()

def delete_todo(todo_id):
    todo = Tasks.query.filter_by(id=todo_id).first()
    db.session.delete(todo)
    db.session.commit()

def update_todo(todo_id, done):
    todo_done = not bool(done)
    todo = Tasks.query.filter_by(id=todo_id).first()
    todo.done = todo_done
    db.session.commit()

def get_todos(user_id):
    return Tasks.query.filter_by(user_id=user_id).all()

def get_todos_full():
    return Tasks.query.all()
