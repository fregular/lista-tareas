out=$(ls migrations > /dev/null 2>&1)

if [ $? -eq 0 ]; then
    echo "existe migrations"
    docker-compose exec -T web python manage.py db migrate
    docker-compose exec -T web python manage.py db upgrade
else
    echo "no existe migrations"
    docker-compose exec -T web python manage.py db init
    docker-compose exec -T web python manage.py db migrate
    docker-compose exec -T web python manage.py db upgrade
fi
